# == Class: master
#
# Installs an ElasticSearch master
#
class master {

  class { 'elasticsearch':
    config => hiera_hash('master_config'),
  }

  elasticsearch::plugin{
    'mobz/elasticsearch-head':
      module_dir => 'head';
    'royrusso/elasticsearch-HQ':
      module_dir => 'HQ';
    'lmenezes/elasticsearch-kopf':
      module_dir => 'kopf';
  }

    # Kibana 4

  $kibana_version = 'kibana-4.0.2-linux-x64'
  $kibana_link = "https://download.elasticsearch.org/kibana/kibana/${kibana_version}.tar.gz"

  ensure_packages('wget')

  exec { 'install_kibana4':
    command  => "/usr/bin/wget -P /tmp ${kibana_link} && /bin/tar -xvzf /tmp/${kibana_version}.tar.gz -C /usr/share -o",
    creates  => "/usr/share/${kibana_version}",
    require  => Package['wget']
  } ->
  file { '/etc/systemd/system/kibana4.service':
    content => template('/etc/puppet/templates/elasticsearch/kibana4.service.erb')
  } ->
  service { 'kibana4':
    ensure      => running,
    hasstatus   => true,
    hasrestart  => true,
    enable      => true,
  }

  flume::tail { "tail-es-$::hostgroup_2":  # give a name without spaces otherwise it will fail
    files => ["/var/log/elasticsearch/$::hostgroup_2.log"],  # your list of files to tail
    out   => {
      'es1' => {
        type => 'es',
        hostNames => "$::hostgroup_2-master-0.cern.ch,$::hostgroup_2-master-1.cern.ch",  # coma separated list of ES nodes with no spaces in-between
        indexName => 'logs',  # name of the index in ES
        clusterName => $::hostgroup_2,  # name of your ES cluster
      }
    }
  }
}
