ElasticSearch cluster orchestration
===================================

Structure
---------

```
./clones/                   Git submodules for required Puppet modules
./hieradata/                Hiera data tree
./hieradata/module_names/   Symlinks to modules data
./hieradata/common.yaml     General purpose hiera file
./manifests/
./manifests/site.pp         Base site.pp
./manifests/hacks.pp        Compilation of hacks to workaround Cern
./modules/                  Symlinks to modules code
./hiera.yaml                Hiera config file
./puppet.conf               Puppet config file
```

How to run Puppet
-----------------

1. Clone into `/etc/puppet`
```
git clone ...
```
2. Initialize submodule
```
git submodule update --init --recursive
```
3. Run puppet apply
```
puppet apply -tv --modulepath=/etc/puppet/modules/ /etc/puppet/manifests/
```
