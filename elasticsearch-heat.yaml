heat_template_version: 2013-05-23

description: Simple template to deploy a Puppet managed ElasticSearch cluster.

parameters:
  cluster_name:
    type: string
    description: ES cluster name
    constraints:
    - allowed_pattern: "[a-zA-Z0-9-]+"
  es_master_nodes:
    type: number
    description: Number of master nodes for the ES cluster
    default: 2
    constraints:
    - range: {min: 1, max: 4}
  es_master_flavor:
    type: string
    description: Flavor for the masters
    default: m1.medium
    constraints:
    - allowed_values: [ m1.small, m1.medium, m1.large ]
  es_data_nodes:
    type: number
    description: Number of master nodes for the ES cluster
    default: 2
    constraints:
    - range: {min: 1, max: 10}
  es_data_flavor:
    type: string
    description: Flavor for the data nodes
    default: m1.medium
    constraints:
    - allowed_values: [ m1.small, m1.medium, m1.large ]
  image_name:
    type: string
    description: Image used for the machines
    default: "CC7 Extra - x86_64 [2015-02-10]"
resources:
  master_nodes:
    type: OS::Heat::ResourceGroup
    properties:
        count: {get_param: es_master_nodes}
        resource_def:
            type: OS::Nova::Server
            properties:
              name:
                str_replace:
                    template: $cluster_name-master-%index%
                    params:
                        $cluster_name: {get_param: cluster_name}
              key_name: testing
              image: {get_param: image_name}
              flavor: {get_param: es_master_flavor}
              user_data:
                str_replace:
                        params:
                            $cluster_name: {get_param: cluster_name}
                            $node_name: es-master-%index%
                            $master_node: "true"
                            $data_node: "false"
                        template: {get_file: es_init.sh}
  data_nodes:
    type: OS::Heat::ResourceGroup
    properties:
        count: {get_param: es_data_nodes}
        resource_def:
            type: OS::Nova::Server
            properties:
              name:
                str_replace:
                    template: $cluster_name-data-%index%
                    params:
                        $cluster_name: {get_param: cluster_name}
              key_name: testing
              image: {get_param: image_name}
              flavor: {get_param: es_data_flavor}
              user_data:
                str_replace:
                        params:
                            $cluster_name: {get_param: cluster_name}
                            $node_name: es-data-%index%
                            $master_node: "false"
                            $data_node: "true"
                        template: {get_file: es_init.sh}
outputs:
  masters:
    description: The name of the created masters
    value: {get_attr: [master_nodes, name]}
  data_nodes:
    description: The name of the created data nodes
    value: {get_attr: [data_nodes, name]}
  kibana_url:
    description: The kibana URL. Access it through a web browser
    value:
      str_replace:
        template: "http://%name%.cern.ch:5601"
        params:
          "%name%": {get_attr: [master_nodes, name, 0]}
  es_url:
    description: The ES URL. Do `curl http://<url>/_cat/nodes` to see all the
        nodes of the cluster.
    value:
      str_replace:
        template: "http://%name%.cern.ch:9200"
        params:
          "%name%": {get_attr: [master_nodes, name, 0]}
  elastichq_url:
    description: URL for the elasticHQ monitoring plugin
    value:
      str_replace:
        template: "http://%name%.cern.ch:9200/_plugin/HQ/?url=http://%name%.cern.ch:9200"
        params:
          "%name%": {get_attr: [master_nodes, name, 0]}
  head_url:
    description: URL for the head plugin
    value:
      str_replace:
        template: "http://%name%.cern.ch:9200/_plugin/head/"
        params:
          "%name%": {get_attr: [master_nodes, name, 0]}
  kopf_url:
    description: URL for the Kopf plugin
    value:
      str_replace:
        template: "http://%name%.cern.ch:9200/_plugin/kopf/"
        params:
          "%name%": {get_attr: [master_nodes, name, 0]}
